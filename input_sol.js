/**
 * Created by fliwa on 15/11/16.
 */

/*Simple app to get user input from command line*/

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/fsf3_db');
//mongoose.connect('mongodb://admin:admin@ds155087.mlab.com:55087/fsf');
//mongodb://<dbuser>:<dbpassword>@ds155087.mlab.com:55087/fsf

console.log('Welcome to my node app');


var key = '';
var value = '';
var number= '';

process.argv.forEach(function(val, index, array){
    console.log(index + ': ' + val);
    if(index == 2) {
        key = val;
    }
    if(index == 3) {
        value = val;
    }
});
console.log('inputs: ' + key + ':' + value);

// sample run
// [0] = node
// [1] = app name
// [node,appname,parameters...]
// node input.js name jill



var query = {};

if(key == '_id')
    query[key] = mongoose.Types.ObjectId(value);
else if(key == 'age')
    query[key] = value;
else
    query[key] = value;


var query2 = {  $text: { $search: "java co\n" } };
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    //console.log('DB connected!');
});


var Schema = mongoose.Schema;


//Defining Person schema
var PersonSchema  = new Schema({
    name:String,
})


//               mongoose.model(<collection name>, <schema>)
var PersonModel = mongoose.model('Group',PersonSchema);

console.log('My QUERY :' + JSON.stringify(query));
PersonModel.find(query,function(err,res){

    if(err)
        console.log(err)
    else
    {
        if( res.length != 0)
            console.log('Found! ' + res);
        else
            console.log('Person does not exist!');

    }
})
