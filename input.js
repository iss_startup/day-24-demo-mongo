/**
 * Created by fliwa on 15/11/16.
 */

/*Simple app to get user input from command line*/

console.log('Welcome to my node app');


var key = '';
var value = '';

process.argv.forEach(function(val, index, array){
    console.log(index + ': ' + val);
    if(index == 2) {
        key = val;
    }
    if(index == 3) {
        value = val;
    }
});
console.log('inputs: ' + key + ':' + value);

// sample run
// [0] = node
// [1] = app name
// [node,appname,parameters...]
// node input.js name jill

//DB connection using mongoose
//Use a model.find (<query>)