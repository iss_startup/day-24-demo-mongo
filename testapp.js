/**
 * Created by fliwa on 15/11/16.
 */

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/fsf3_db');
//mongoose.connect('mongodb://<DB_USER>:<DB_PWD>@ds155087.mlab.com:55087/fsf');
//mongoose.connect('mongodb://admin:admin123@ds155087.mlab.com:55087/fsf_mlab'); //mLab uri access



var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    //console.log('DB connected!');
});


var Schema = mongoose.Schema;

var Pet   = new Schema(
    {
        name:String
    }
)

//Defining Person schema
var PersonSchema  = new Schema({
    name:String,
    pets:[Pet]
})

//Adding methods to your schema
PersonSchema.methods.speak = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting);
}

PersonSchema.methods.shout = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting+ greeting);
}

//mongoose.model(<Model_NAME>, <schema>,<COLLECTIONS_NAME>)
var PersonModel = mongoose.model('Group',PersonSchema,'grouptests');

var person = new PersonModel({name:'Francisco'});
//var person = new PersonModel({name:'Jill'});


/*PersonModel.create({name:'Lin'}, function(err, doc) {
    if (err) return next(err);

});*/
//Process User input
// Process.argv....

var query = {}

//key-value pair query string
query['name'] = 'Jill'



person.pets.push({name:'meow'} );

person.save(function(err){

});

console.log('Person:' + person);

person.speak();
person.shout();





