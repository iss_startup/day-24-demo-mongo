var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./routes');
var config   = require('./config')

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


//ROUTES

app.get('/api/todos/all', routes.all);
app.get('/api/todos/findOne/:id', routes.findOne);
app.post('/api/todos/create', routes.add);
app.post('/api/todos/delete/:id', routes.remove);
app.post('/api/todos/edit/:id', routes.edit);


app.listen(config.appPort, function() {
    console.log('listening on http://localhost:'+ config.appPort);
});