
var config = require('../config')
var mongoose = require('mongoose');

mongoose.connect(config.mongodbUrl);

require('../models/todo')();

var Todo = mongoose.model('Todo');



module.exports = {
    all: function(req, res){
        Todo.find(function(err, docs) {
            if (err) return next(err);
            res.send(docs);
        });
        //res.send('All todos')
    },
    findOne: function(req, res){
        console.log('Searching for document id :' + req.params.id);

        var id = req.params.id;
        Todo.find({_id:id},function(err,doc){
          res.send(doc);
        })
    },
    add: function(req, res){
        console.log('Requesting to add tasks: ' )
        console.log(JSON.stringify(req.body));
        Todo.create(req.body, function(err, doc) {
            if (err) return next(err);
            res.send(doc);
        });
        console.log('Todo created')
    },
    remove: function(req, res){

        tid = mongoose.Types.ObjectId(req.params.id)  //todo_id

        Todo.remove({'_id':tid}, function(err, doc) {
            if (err) return next(err);
            res.send(doc);
        });

        console.log('Todo deleted')
    },
    edit: function(req, res){                      // return the updated document

        console.log('Todo ' + req.body);

        var id = req.params.id;
        var newValue = req.body;

        Todo.findByIdAndUpdate(id, { $set: newValue}, { new: true }, function (err, tank) {
            if (err) return handleError(err);
            res.send(tank);
        });

        console.log('Todo ' + req.params.id + ' updated')
    }
};
