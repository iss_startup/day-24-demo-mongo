// import the necessary modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create an export function to encapsulate the model creation
module.exports = function() {
    // define schema
    var TodoSchema = new Schema({
        desc: String,           //Task description
        tag : String,           //Tags      : #personal, #work, #family
        prio: Number,     //Priority  : scale 0-100
        status: String,
        postDate: {
            type:Date,
            default: Date.now()
        }
    });
    mongoose.model('Todo', TodoSchema,'todos');
};
