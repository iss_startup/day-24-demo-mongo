//include moongose in project
var mongoose = require('mongoose');


mongoose.Promise = global.Promise;

//connect to db
mongoose.connect('mongodb://localhost/fsf3');

//connection instance
//var db = mongoose.connection;
//db.on('error', console.error.bind(console, 'connection error:'));
//db.once('open', function() {
 //   console.log('DB connected!');

//});

//With Mongoose, everything is derived from a Schema.

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var Pet = new Schema({
    name: String
});


var personSchema = mongoose.Schema({
    name: String,
    pets: [Pet]
});



personSchema.methods.speak = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting);
}

//Compile schema into a model( a model is class for documents

var Person = mongoose.model('Person',personSchema);


//Create instance of Person model
var firstChild = new Person({name:'Yohan'});

firstChild.pets.push({name:'Moooo'});

//Write to MongoDB
firstChild.save(function(err,firstChild){
    firstChild.speak();
})

//Read from MongoDB

//find all people
Person.find(function (err, persons) {
    if (err) return console.error(err);
    console.log(persons);
})

//find one person using Query

Person.find({_id:  mongoose.Types.ObjectId('582a3c7dfa742e112d6a7c74')},function(err,persons){
    if (err) return console.error(err);

    console.log('Found results by Id: '+ persons);
});


Person.find({name:'Yohan'},function(err,persons){
    if (err) return console.error(err);

    console.log('Found results by name: '+ persons);
});





