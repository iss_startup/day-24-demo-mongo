/**
 * Created by fliwa on 16/11/16.
 */

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/fsf3_db');

require('./user_model.js')();

var User = mongoose.model('User');

exports.home = function(req, res, next) {
    User.find(function(err, docs) {
        if (err) return next(err);
        res.send(docs);
    });
};

exports.modelName = function(req, res) {
    console.log('Accessing route /name');
    res.send('my model name is ' + User.modelName);
};

exports.insert = function(req, res, next) {
    console.log('Requesting to insert data: ' )
    console.log(JSON.stringify(req.body));
    User.create(req.body, function(err, doc) {
        if (err) return next(err);
        res.send(doc);
    });
};