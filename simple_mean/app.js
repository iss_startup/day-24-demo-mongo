/**
 * Created by fliwa on 16/11/16.
 */

var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./routes');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.get('/', routes.home);
app.post('/insert', routes.insert);
app.get('/name', routes.modelName);


app.listen(8000, function() {
    console.log('listening on http://localhost:8000');
});