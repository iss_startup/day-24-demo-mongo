/**
 * Created by fliwa on 16/11/16.
 */



// import the necessary modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create an export function to encapsulate the model creation
module.exports = function() {
    // define schema
    var UserSchema = new Schema({
        name: String,
        age: Number,
        birthday: Date,
        location: String,
        job: String
    });
    mongoose.model('User', UserSchema);
};


